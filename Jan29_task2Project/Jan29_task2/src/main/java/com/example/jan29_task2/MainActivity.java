package com.example.jan29_task2;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static android.provider.Contacts.People.*;
import static android.provider.MediaStore.Images.Media.*;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    Button Camera;
    Button Flash;
    Button Contact;
    EditText editText;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Camera = (Button) findViewById(R.id.buttonCamera);
        Camera.setOnClickListener(this);

        Flash = (Button) findViewById(R.id.buttonFlash);
        Flash.setOnClickListener(this);

        Contact = (Button) findViewById(R.id.buttonContact);
        Contact.setOnClickListener(this);
    }

    private static final int CAMERA_PIC_REQUEST = 7;
    private static final int CONTACT_PIC_REQUEST = 8;
    private static final int GALLERY_REQUEST = 9;
    Intent intent;

    @Override
    public void onClick(View view) {
        switch(view.getId()){

            case R.id.buttonCamera:
                intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
                break;

            case R.id.buttonContact:
                intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
              //  intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, CONTACT_PIC_REQUEST);
                break;

            case R.id.buttonFlash:
                intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_REQUEST);
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        editText = (EditText) findViewById(R.id.editText);
        imageView = (ImageView) findViewById(R.id.imageView);
        if (requestCode == CAMERA_PIC_REQUEST) {
            Bitmap bm = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(bm);
            editText.setText("Картинка с камеры");
        }
        if (requestCode == GALLERY_REQUEST) {
            Bitmap bm;
            if (data != null){
           /* Uri selectedImage = data.getData();
            imageView.setImageURI(selectedImage);
         //  bm = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                imageView.setImageBitmap(bm);*/
            InputStream stream = null;
            try {
                stream = getContentResolver().openInputStream(data.getData());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bm = BitmapFactory.decodeStream(stream);
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(bm);

            editText.setText("Картинка из галереи");
            } else {
                editText.setText("Галерея пуста");
        }
        }
        if (requestCode == CONTACT_PIC_REQUEST) {
            Cursor cursor = getContentResolver().query(data.getData(),
                    new String[] {ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                String name = cursor.getString(columnIndex);
                editText = (EditText) findViewById(R.id.editText);
                editText.setText("Name of the chosen contact is "+ name);
            }

        }
    }

    //Menu. don't touch this

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
