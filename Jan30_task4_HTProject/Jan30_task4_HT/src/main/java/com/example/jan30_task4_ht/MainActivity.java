package com.example.jan30_task4_ht;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity /*implements View.OnClickListener*/ {
    Button countButton;
    EditText editText;
    int n;
    private NewThread FACTORIAL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* countButton = (Button) findViewById(R.id.button);
        countButton.setOnClickListener(this);*/

       FACTORIAL = (NewThread) getLastNonConfigurationInstance();
        if (FACTORIAL == null){
            FACTORIAL = new NewThread();
            FACTORIAL.execute(10);
        }

    }

    public Object onRetainNonConfigurationInstance() {
        return this;
    }

   /* @Override
    public void onClick(View view) {
        switch (view.getId()){
            case (R.id.button):
                FACTORIAL.execute(Integer.parseInt(editText.getText().toString()));
                countButton.setEnabled(false);
        }
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    class NewThread extends AsyncTask<Integer, Integer, Void> {

        @Override
        protected Void doInBackground(Integer... integers) {
            n = integers [0];
            try {
                for (int j = 1; j < n; j++ ){
                    Thread.sleep(1000);
                    publishProgress(n-j);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            editText.setText("До конца отработки осталось " + values[0] + "секунд");
        }


        protected void onPostExecute(Void result) {
           editText.setText("Задача отработала");
        }
    }
}
