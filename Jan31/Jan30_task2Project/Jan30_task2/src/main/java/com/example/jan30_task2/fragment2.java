package com.example.jan30_task2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev3 on 31.01.14.
 */
public class fragment2 extends Fragment {

    public fragment2() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_2, container, false);
        Button myButton = null;
        if (rootView != null) {
            myButton = (Button) rootView.findViewById(R.id.button2);
        }
        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((MainActivity) getActivity()).onClick(view);
            }
        });
        return rootView;
    }
}