package com.example.jan30_task2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jan30_task2.R;

public class fragment3 extends Fragment {

    public fragment3() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_3, container, false);
        Button myButton = null;
        if (rootView != null) {
            myButton = (Button) rootView.findViewById(R.id.button3);
        }
        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((MainActivity) getActivity()).onClick(view);
            }
        });
        return rootView;
    }
}
