package com.example.jan30_task2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev3 on 31.01.14.
 */
public class fragment1 extends Fragment {
    public View rootView;
    public fragment1() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_1, container, false);
        Button myButton = null;
        if (rootView != null) {
            myButton = (Button) rootView.findViewById(R.id.button1);
        }
        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((MainActivity) getActivity()).onClick(view);
            }
        });
        return rootView;
    }
}
