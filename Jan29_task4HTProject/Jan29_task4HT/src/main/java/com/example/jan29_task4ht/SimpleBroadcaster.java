package com.example.jan29_task4ht;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by dev3 on 31.01.14.
 */
public class SimpleBroadcaster extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Сообщение получено", Toast.LENGTH_SHORT).show();
    }
}
