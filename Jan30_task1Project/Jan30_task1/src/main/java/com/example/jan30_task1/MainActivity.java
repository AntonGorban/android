package com.example.jan30_task1;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.os.SystemClock.sleep;
import static java.lang.Integer.*;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Button countButton;
    EditText editText;
    final Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countButton = (Button) findViewById(R.id.button);
        countButton.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editText);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case (R.id.button):
                if ((editText.getText().toString().trim().length() == 0) || (Integer.parseInt(editText.getText().toString()) < 0)){
                    Toast.makeText(this, "Введите корректные данные", Toast.LENGTH_SHORT).show();
                    break;
                }
                    final int n = Integer.parseInt(editText.getText().toString());
                    final int[] cnt = {1};
                    Thread NewThread = new Thread(new Runnable(){


                        @Override
                        public void run() {
                            for (int j=1; j<=n; j++){
                                cnt[0] *= j;
                            }
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "Факториал числа равен " + cnt[0], Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                    NewThread.start();
                    break;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
