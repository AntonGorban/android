package com.example.jan30_task3;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@TargetApi(19)
public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Integer>, View.OnClickListener {

    Button countButton;
    EditText editText;
    private static int LOADER_ID = 1;
    Loader<Integer> loader;
    Bundle bndl = new Bundle();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countButton = (Button) findViewById(R.id.button);
        countButton.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editText);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case (R.id.button):
                if ((editText.getText().toString().trim().length() == 0)){
                    Toast.makeText(this, "Введите корректные данные", Toast.LENGTH_SHORT).show();
                    break;
                }


                bndl.putInt("Number", Integer.parseInt(editText.getText().toString()));
                getLoaderManager().initLoader(LOADER_ID++, bndl, this);
                loader.forceLoad();
                break;

        }
    }


    @Override
    public Loader<Integer> onCreateLoader(int i, Bundle bundle) {
       // if (i == LOADER_ID) {
            loader = new Factorial(this, bundle);
        // }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Integer> integerLoader, Integer integer) {
        Toast.makeText(MainActivity.this, "Факториал числа равен " + integer, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoaderReset(Loader<Integer> integerLoader) {

    }


    // =========================== Menu ========================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
