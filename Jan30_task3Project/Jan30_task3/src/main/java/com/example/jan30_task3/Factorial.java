package com.example.jan30_task3;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Loader;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by dev3 on 30.01.14.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Factorial extends Loader<Integer>{
    int n;
    int cnt = 1;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public Factorial(Context context, Bundle bndl) {
        super(context);
        n = bndl.getInt("Number");
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();

    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        for (int j = 1; j <= n; j++) cnt *= j;
        deliverResult(cnt);
    }


}
