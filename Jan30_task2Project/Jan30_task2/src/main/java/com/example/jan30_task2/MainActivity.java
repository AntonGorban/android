package com.example.jan30_task2;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    Button countButton;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countButton = (Button) findViewById(R.id.button);
        countButton.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editText);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case (R.id.button):
                if ((editText.getText().toString().trim().length() == 0) || ( Integer.parseInt(editText.getText().toString()) < 0)){
                    Toast.makeText(this, "Введите корректные данные", Toast.LENGTH_SHORT).show();
                    break;
                }

                NewThread FACTORIAL = new NewThread();
                FACTORIAL.execute(Integer.parseInt(editText.getText().toString()));
                break;
        }
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class NewThread extends AsyncTask<Integer, Integer, Integer> {
            int cnt = 1;

        @Override
        protected Integer doInBackground(Integer... integers) {
            cnt = 1;
            for (int j = 1; j <= integers[0]; j++) cnt *=j;
            return cnt;
        }


        protected void onPostExecute(Integer result) {
            Toast.makeText(MainActivity.this, "Факториал числа равен " + result, Toast.LENGTH_SHORT).show();
        }
    }

}
