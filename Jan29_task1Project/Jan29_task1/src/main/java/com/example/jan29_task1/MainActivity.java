package com.example.jan29_task1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Button SMS;
    Button Google;
    Button Mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SMS = (Button) findViewById(R.id.buttonSMS);
        SMS.setOnClickListener((View.OnClickListener) this);

        Google = (Button) findViewById(R.id.buttonGoogle);
        Google.setOnClickListener(this);

        Mail = (Button) findViewById(R.id.buttonMail);
        Mail.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSMS:
                Intent openSMS = new Intent (Intent.ACTION_VIEW, Uri.parse("sms:"));
                startActivity(openSMS);
                break;
            case R.id.buttonGoogle:
                Intent openGoogle = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(openGoogle);
                break;
            case R.id.buttonMail:
                Intent openMail = new Intent (Intent.ACTION_VIEW, Uri.parse("mailto:"));
                startActivity(openMail);
                break;
    }
}

} //end of Activity